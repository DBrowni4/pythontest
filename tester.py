import os, sys


# i kept having issues using a bash for loop in the gitlab ci to test python files
# so here's an over-engineered python solution

# sandboxing for our test files, pythonFile is the string file name of the file we wish to run
def container(pythonFile):
    script = ""
    # read the whole file - our programs aren't very big so this works well for our usage
    with open("./tests/" + pythonFile, "r") as filePointer:
        script = filePointer.read()
    # run the code and return the result, should be 0, if not it will be flagged for error
    try:
        exec(script)
    except:
        return 1
    return 0


# finds all the code we want to run
def findFiles():
    stagedFiles = os.listdir("tests")
    pythonFiles = []
    for stagedFile in stagedFiles:
        if stagedFile.endswith(".py"):
            pythonFiles.append(stagedFile)

    return pythonFiles


# container runner with primitive error handling
def runner(pythonFile):
    retval = container(pythonFile)
    if retval != 0:
        print("Error in " + pythonFile + ". Exiting now.")
        sys.exit(retval)

    return 0


# main code
for eachFile in findFiles():
    print("Running: " + eachFile)
    runner(eachFile)
