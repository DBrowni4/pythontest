# PythonTest

Python CI/CD testing environment

## Contributions
Contributions are welcome, imagine this as a virtual Python playground. As long as you don't violate GitLab ToS and format rules I don't care what you do. If you put python code into the tests folder, it will run.

## Format Rules
### Input
* Don't take input from console. This will clog the pipeline. Take input from hard-coded files in tests/data instead.