
# manages factorial cache and job handling
class FactorialObject:
    def __init__(self):
        self.__cache = {}
        self.loadCache()

    # actually does the job
    def calculate(self, n):
        if n < 0:
            return 1

        if n not in self.__cache.keys():
            self.__cache[n] = n * self.calculate(n - 1)

        self.saveCache()
        return self.__cache[n]

    def saveCache(self):
        data = ""
        for eachKey in self.__cache.keys():
            data += str(eachKey) + ":" + str(self.__cache[eachKey]) + '\n'

        with open("./data/cachedfactorialcache.txt",'w') as filePointer:
            filePointer.write(data)

    def loadCache(self):
        with open("./data/cachedfactorialcache.txt", 'r') as filePointer:
            data = filePointer.readline()
            while(data != ""):
                fields = data.split(':')
                self.__cache[int(fields[0])] = int(fields[1])
                data = filePointer.readline()


calculator = FactorialObject()
n = -1
with open("./data/cachedfactorial.txt", "r") as filePointer:
    data = filePointer.readline()
    print(data)
    n = int(data)

value = calculator.calculate(n)

print("Factorial of " + str(n) + " is " + str(value))