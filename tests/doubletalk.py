import random

def getTalker(phrase):
    def outFunc(times):
        while times > 0:
            print(phrase)
            times -= 1

    return outFunc


getTalker("How many times do I print?")(random.randint(1, 10))